PRODUCT_PACKAGES := \
    Email \
    IM \
    VoiceDialer \
    SdkSetup


$(call inherit-product, build/target/product/generic_with_google.mk)

#Enabling Ring Tones
include frameworks/base/data/sounds/OriginalAudio.mk

# Overrides
PRODUCT_BRAND := qcom
PRODUCT_NAME := qsd8250_surf
PRODUCT_DEVICE := qsd8250_surf
